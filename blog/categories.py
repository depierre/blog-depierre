"""
    This is the categorie module which provides all the routes, functions, etc.
    to manage the categories.
"""


from flask import request
from flask import session
from flask import g
from flask import redirect
from flask import url_for
from flask import render_template
from flask import flash
from blog import app


@app.route('/categories/')
def categories():
    """Get all the categories to show them in categories.html"""

    cur = g.db.execute("SELECT id_categorie, label FROM categories")
    categories = [dict(id_cat=row[0], label=row[1]) for row in cur.fetchall()]

    return render_template('categories.html', categories=categories)


@app.route('/categories/<int:id_cat>')
def show_categorie(id_cat):
    """Get all the articles linked to the categorie 'id_cat'."""

    cur = g.db.execute('SELECT id_article, title, summary, lang \
                        FROM articles \
                        WHERE id_categorie=? \
                        ORDER BY id_article DESC', [id_cat])
    articles = [dict(id_a=row[0], title=row[1], summary=row[2], lang=row[3]) \
                for row in cur.fetchall()]

    return render_template('articles.html', articles=articles)


@app.route('/categories/add_cat', methods=['POST'])
def add_cat():
    """add a new categorie to the db"""

    if session.get('logged_in'):
        g.db.execute('INSERT INTO categories VALUES (?, ?)', \
                    (None, request.form['label']))
        g.db.commit()
        if session['en']:
            flash('New categorie successfully created')
        else:
            flash('Nouvelle categorie creee avec succes')
    else:
        if session['en']:
            flash('You must be logged in to add a categorie')
        else:
            flash('Vous devez etre connecte pour creer une categorie')

    return redirect(url_for('categories'))


@app.route('/categories/edit_cat', methods=['POST'])
def edit_cat():
    """edit a categorie in the db"""

    if session.get('logged_in'):
        g.db.execute('UPDATE categories SET label=? WHERE id_categorie=?', \
                    (request.form['label'], request.form['id_cat']))
        g.db.commit()
        if session['en']:
            flash('Categorie successfully edited')
        else:
            flash('Categorie editee avec succes')
    else:
        if session['en']:
            flash('You must be logged in to edit a categorie')
        else:
            flash('Vous devez etre connecte pour editer une categorie')

    return redirect(url_for('categories'))


@app.route('/categories/del_cat/<int:id_cat>')
def delete_cat(id_cat):
    """delete a categorie in the db"""

    if session.get('logged_in'):
        g.db.execute('DELETE FROM categories WHERE id_categorie=?', [id_cat])
        g.db.commit()
        if session['en']:
            flash('Categorie successfully deleted')
        else:
            flash('Categorie supprimee avec succes')
    else:
        if session['en']:
            flash('You must be logged in to delete a categorie')
        else:
            flash('Vous devez etre connecte pour supprimer une categorie')

    return redirect(url_for('categories'))

