drop table if exists categories;
drop table if exists articles;
drop table if exists categories_per_article;

create table categories (
    id_categorie integer primary key autoincrement,
    label string not null);

create table articles (
    id_article integer primary key autoincrement,
    id_categorie integer,
    title string not null,
    summary string,
    text string not null,
    lang string,
    tags string,
    updated timestamp not null,
    foreign key(id_categorie) references categories(id_categorie));

create table categories_per_article (
    id_link integer primary key autoincrement,
    id_categorie integer,
    id_article integer,
    foreign key(id_categorie) references categories(id_categorie),
    foreign key(id_article) references articles(id_article));
