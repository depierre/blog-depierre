from flask import Flask
from flaskext.babel import Babel


app = Flask(__name__)
app.config.from_pyfile('config.cfg')
babel = Babel(app)


import blog.main
import blog.articles
import blog.categories
import blog.admin
import blog.login
import blog.language
import blog.search
