"""
    This is the article module which provides all the routes, functions, etc.
    to manage the articles.
"""


from flask import request
from flask import g
from flask import redirect
from flask import url_for
from flask import render_template
from flask import flash
from blog import app
from datetime import date


@app.route('/articles/')
def articles(articles = None):
    """Get all the articles to show them in articles.html"""

    cur = g.db.execute('SELECT id_article, title, lang, summary \
                        FROM articles\
                        ORDER BY id_article DESC')
    articles = [dict(id_a=row[0], title=row[1], lang=row[2], summary=row[3]) \
                for row in cur.fetchall()]

    return render_template('articles.html', articles=articles)


@app.route('/articles/<int:id_art>')
def show_article(id_art):
    """Get the only article with the id 'id_art'
    and show it in article.html."""

    cur = g.db.execute('SELECT id_article, id_categorie, title, summary, \
            text, lang, tags, updated \
            FROM articles WHERE id_article=?', [id_art])
    article = [dict(id_a=row[0], id_c=row[1], title=row[2], summary=row[3], \
            text=row[4], lang=row[5], tags=row[6], updated=row[7]) \
            for row in cur.fetchall()]

    return render_template('article.html', article=article)


@app.route('/articles/add_art', methods=['POST'])
def add_art():
    """add a new article to the db"""

    if session.get('logged_in'):
        g.db.execute('INSERT INTO articles \
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)', \
                    (None, request.form['id_cat'], \
                    request.form['title'], request.form['summary'], \
                    request.form['text'], request.form['lang'], \
                    request.form['tags'], date.today()))
        g.db.commit()
        if session['en']:
            flash('New article successfully posted')
        else:
            flash('Nouvel article ajoute avec succes')
    else:
        if session['en']:
            flash('You must be logged in to add an article')
        else:
            flash('Vous devez etre connecte pour ajouter un article')

    return redirect(url_for('articles'))


@app.route('/articles/edit_art', methods=['POST'])
def edit_art():
    """edit an article in the db"""

    if session.get('logged_in'):
        g.db.execute('UPDATE articles SET title=?, summary=?,\
                    text=?, id_categorie=?, tags=?, lang=?, updated=? \
                    WHERE id_article=?',
                    (request.form['title'], request.form['summary'],
                    request.form['text'], request.form['id_cat'],
                    request.form['tags'], request.form['lang'],
                    request.form['updated'], request.form['id_art']))
        g.db.commit()
        if session['en']:
            flash('Article successfully edited')
        else:
            flash('Article edite avec succes')
    else:
        if session['en']:
            flash('You must be logged in to edit an article')
        else:
            flash('Vous devez etre connecte pour editer un article')

    return redirect(url_for('articles'))


@app.route('/articles/del_art/<int:id_art>')
def delete_art(id_art):
    """delete an article in the db"""

    if session.get('logged_in'):
        g.db.execute('DELETE FROM articles WHERE id_article=?', [id_art])
        g.db.commit()
        if session['en']:
            flash('Article successfully deleted')
        else:
            flash('Article supprime avec succes')
    else:
        if session['en']:
            flash('You must be logged in to delete an article')
        else:
            flash('Vous devez etre connecte pour supprimer un article')

    return redirect(url_for('articles'))

