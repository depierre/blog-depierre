""""
    This is the language python file of the blog project.
    It allows the user to define if he wants the french version or
    the english one.
"""


from flask import session
from flask import redirect
from flask import url_for
from flask import flash
from blog import app


@app.route('/language/en/')
def lang_en():
    """Set the language to en"""

    session['en'] = True
    flash('The version is now set to english')

    return redirect(url_for('index'))

@app.route('/language/fr/')
def lang_fr():
    """Set the language to fr"""

    session['en'] = False
    flash('La langue choisie est maintenant le francais')

    return redirect(url_for('index'))
