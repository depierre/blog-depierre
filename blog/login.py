"""
    This is the login python file of the blog project.
    It allows the admin to login and logout.
"""


from flask import request
from flask import session
from flask import redirect
from flask import url_for
from flask import flash
from blog import app


@app.route('/login/', methods=['POST'])
def login():
    """Log the user as admin if username and password are correct."""

    if request.form['username'] != app.config['USERNAME'] \
    or request.form['password'] != app.config['PASSWORD']:
        if 'en' in session:
            if session['en']:
                error = 'Invalid username/password'
        else:
            error = 'Nom d\'utilisateur/Mot de passe incorrecte'
        return render_template('admin.html', error=error)
    else:
        session['logged_in'] = True
        if 'en' in session:
            if session['en']:
                flash('You were logged in')
        else:
            flash('Vous etes maintenant connecte')
        return redirect(url_for('admin'))


@app.route('/logout/')
def logout():
    """Log out the admin."""

    session.pop('logged_in', None)
    if 'en' in session:
        if session['en']:
            flash('You were logged out')
    else:
        flash('Vous etes maintenant deconnecte')

    return redirect(url_for('index'))

