"""
    This is the search module which process search requests from the user.

"""


from flask import request
from flask import g
from flask import render_template
from blog import app


@app.route('/search/')
def search():
    """Display the template search.html."""

    cur = g.db.execute('SELECT id_categorie, label FROM categories')
    categories = [dict(id_c=row[0], label=row[1]) for row in cur.fetchall()]

    return render_template('search.html', categories=categories)


@app.route('/search/res/', methods=['POST'])
def global_search():
    """Process the global search requests."""

    categorie = request.form['categorie']
    search = request.form['search'].split()

    # Get all the related articles
    if not categorie:
        cur = g.db.execute('SELECT id_article, id_categorie, title, \
                summary, lang, tags \
                FROM articles ORDER BY id_article DESC')
    else:
        cur = g.db.execute('SELECT id_article, id_categorie, title, \
                summary, lang, tags \
                FROM articles WHERE \
                id_categorie=(SELECT id_categorie \
                            FROM categories \
                            WHERE label=?) \
                ORDER BY id_article DESC', \
                [categorie])
    articles = [dict(id_a=row[0], id_c=row[1], title=row[2], summary=row[3], \
            lang=row[4], tags=row[5]) for row in cur.fetchall()]

    result = []
    for keyword in search:
        for article in articles:
            if article not in result:
                if keyword in article['title'] or keyword in article['tags']:
                    result.append(article)

    return render_template('articles.html', articles=result)


@app.route('/search/tag/<tag>')
def tag_search(tag = None):
    """Process the tag search requests."""

    result = []
    if tag:
        search = tag
        cur = g.db.execute('SELECT id_article, id_categorie, title, \
                summary, lang, tags \
                FROM articles ORDER BY id_article DESC')
        articles = [dict(id_a=row[0], id_c=row[1], title=row[2], \
                summary=row[3], lang=row[4], tags=row[5]) \
                for row in cur.fetchall()]

        for article in articles:
            if article not in result:
                if tag in article['tags']:
                    result.append(article)

    return render_template('articles.html', articles=result)
