"""
    This is the main python file of the blog project.
    It's the entry-point of the project.
"""


# General
import sqlite3
from flask import request
from flask import g
from flask import render_template
from flaskext.babel import gettext
from flaskext.babel import ngettext
from contextlib import closing
# Perso
from blog import app
from blog import babel


# Translation
@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(['en', 'fr'])


# Database
def connect_db():
    return sqlite3.connect('/tmp/blog-depierre.db')


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    g.db.close()


# Main app
@app.route('/')
def index():
    """Index of the blog.

    Get the last 5 articles and all the categories to show them
    in the index."""


    cur = g.db.execute('SELECT id_article, title, lang, summary \
                        FROM articles \
                        ORDER BY id_article DESC')
    articles = [dict(id_a=row[0], title=row[1], lang=row[2], summary=row[3]) \
                for row in cur.fetchall()]

    cur = g.db.execute('SELECT id_categorie, label FROM categories')
    categories = [dict(id_cat=row[0], label=row[1]) for row in cur.fetchall()]

    return render_template('index.html',\
                            articles=articles,\
                            categories=categories)


@app.route('/about/')
def about():
    """Render template about.html"""

    return render_template('about.html')


@app.route('/links/')
def links():
    """Render template links.html"""

    return render_template('links.html')


@app.route('/license/')
def license():
    """Render template license.html"""

    return render_template('license.html')
