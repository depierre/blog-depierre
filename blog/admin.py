"""
    This is the admin module which provides all the routes, functions, etc.
    to manage the admin panel.
"""


from flask import Flask, request, session, g, redirect, url_for, \
        abort, render_template, flash
from blog import app


@app.route('/admin/')
def admin():
    """Show the admin panel with all the articles and categories."""

    if session.get('logged_in'):
        cur = g.db.execute("SELECT id_article, title, lang, updated \
                FROM articles \
                ORDER BY id_article DESC")
        articles = [dict(id_a=row[0], title=row[1], lang=row[2], \
                summary=row[3]) for row in cur.fetchall()]

        cur = g.db.execute("SELECT id_categorie, label FROM categories")
        categories = [dict(id_cat=row[0], label=row[1]) \
                    for row in cur.fetchall()]

        return render_template('admin.html', \
                            articles=articles, \
                            categories=categories)
    return render_template('admin.html')

